package com.wwmxd;


import com.wwmxd.common.AvoidScan;
import com.wwmxd.common.EnableLogAspect;
import com.wwmxd.config.RibbonConfig;
import com.wwmxd.wwmxdauth.EnablewwmxdAuthClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;


@SpringBootApplication
@EnableDiscoveryClient
@EnablewwmxdAuthClient
@EnableFeignClients
@ServletComponentScan("com.wwmxd.config.druid")
@EnableLogAspect
@RibbonClient(name="wwmxd-auth",configuration = RibbonConfig.class)
@ComponentScan(excludeFilters = {@ComponentScan.Filter(type= FilterType.ANNOTATION,value = {AvoidScan.class})})
public class UserServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}
}
